﻿using System.Collections.Generic;
using DemoAPIMediatr.Models;

namespace DemoAPIMediatr.Repository
{
    public class RepositoryClass
    {
        public List<Employee> Employees { get; } = new()
        {
            new() { Id = 1, Name = "Franco", Surname = "Dugo", Department = "IT"},
            new() { Id = 2, Name = "Juan", Surname = "Rodriguez", Department = "RRHH" },
            new() { Id = 3, Name = "Jose", Surname = "Penia", Department = "ACCOUNT" },
        };
    }
}
