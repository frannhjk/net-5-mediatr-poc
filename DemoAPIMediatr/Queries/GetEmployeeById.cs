﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DemoAPIMediatr.Repository;
using MediatR;

namespace DemoAPIMediatr.Queries
{
    public static class GetEmployeeById
    {
        // Query: Data to Read.
        public record Query(int Id) : IRequest<Response>;

        // Handler: Bussines Logic.
        public class Handler : IRequestHandler<Query, Response>
        {
            private readonly RepositoryClass repository;

            public Handler(RepositoryClass repository)
            {
                this.repository = repository;
            }

            public async Task<Response> Handle(Query request, CancellationToken cancellationToken)
            {
                var employee = repository.Employees.FirstOrDefault(x => x.Id == request.Id);

                return employee is null ? null : new Response(employee.Id, employee.Name, employee.Surname, employee.Department);
            }
        }

        // Response: Data to return.
        public record Response(int Id, string Name, string Surname, string Department);
        
    }
}
